#### V0.0.1

- 支持数据写入硬盘缓存中
- 支持从缓存中读取数据
- 支持从缓存中删除指定key值的缓存文件
- 支持同步缓存文件的操作记录至journal和关闭缓存



